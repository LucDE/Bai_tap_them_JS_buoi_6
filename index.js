// Bài tập thêm buổi 6: In số nguyên tố
var n;
var A = [2];
var B;
function print_prime_number() {
  var count;
  n = Number(dom_id_value("input_number"));
  if (n >= 2) {
    for (i = 3; i <= n; i += 2) {
      count = 0;
      for (j = 1; j <= i; j += 2) {
        if (i % j === 0) {
          count++;
        }
      }
      //chỗ này dùng 1 hoặc 2 dấu bằng kết quả so sánh sẽ cho đúng, xem lại
      if (count === 2) {
        A.push(i);
      }
    }
    B = A.join(", ");
    console.log("n= ", n);
    console.log("A = ", A);
    put_text_into_id("alert", `Số nguyên tố: ${B}`);
    return (A = [2]), ecrease_input_value("input_number");
  } else {
    put_text_into_id("alert", `Không có số nguyên tố nào`);
    return ecrease_input_value("input_number");
  }
}
